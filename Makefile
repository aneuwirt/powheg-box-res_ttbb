#-*- Makefile -*-
## Choose compiler: gfortran,ifort (g77 not supported, F90 constructs in use!)
COMPILER=gfortran
FC=$(COMPILER)
##Choose PDF: native,lhapdf
## LHAPDF package has to be installed separately
PDF=lhapdf
#Choose Analysis: dummy, process specific
ANALYSIS=dummy
#ANALYSIS=default
## For static linking uncomment the following
#STATIC= -static
WITHZLIB=yes

OBJ=obj-$(COMPILER)
OBJDIR:=$(OBJ)

#Path to the OpenLoops installation
OLPATH=$(PWD)/../OpenLoopsStuff/OpenLoops
#Required OL libraries
OLLIBS=ppttbb
OLLIBS+=ppllllbbbb_fac

ifeq ("$(COMPILER)","gfortran")
F77=gfortran -fno-automatic -ffixed-line-length-none -J$(OBJ) -I$(OBJ)
## -fbounds-check sometimes causes a weird error due to non-lazy evaluation
## of boolean in gfortran.
#FFLAGS=-fcheck=all
#FFLAGS= -Wall -Wimplicit-interface -fbounds-check
## For floating point exception trapping  uncomment the following 
#FPE=-ffpe-trap=invalid,zero,overflow,underflow 
## gfortran 4.4.1 optimized with -O3 yields erroneous results
## Use -O2 to be on the safe side
#OPT=-O0
OPT=-O2
## For debugging uncomment the following
#DEBUG= -ggdb 
ifdef DEBUG
OPT=-O0
#FPE=-ffpe-trap=invalid,zero,overflow,underflow
endif
endif

ifeq ("$(COMPILER)","ifort")
F77 = ifort -save  -extend_source  -module $(OBJ)
#CXX = g++
#LIBS = -limf
#FFLAGS =  -checkm
## For floating point exception trapping  uncomment the following 
#FPE = -fpe0
OPT = -O3 #-fast
## For debugging uncomment the following
#DEBUG= -debug -g
ifdef DEBUG
OPT=-O0 
FPE = -fpe0
endif
endif

PWD=$(shell pwd)
WDNAME=$(shell basename $(PWD))
VPATH= ./:../:$(OBJDIR):$(OLPATH)/../

INCLUDE0=$(PWD)
INCLUDE1=$(PWD)/include
INCLUDE2=$(shell dirname $(PWD))/include
FF=$(F77) $(FFLAGS) $(FPE) $(OPT) $(DEBUG) -I$(INCLUDE0) -I$(INCLUDE1) -I$(INCLUDE2)

ifeq ("$(PDF)","lhapdf")
# insert your local path to LHAPDF here:
LHAPDF_CONFIG=lhapdf-config
PDFPACK=lhapdfif.o
LIBSLHAPDF=-Wl,-rpath,$(shell $(LHAPDF_CONFIG) --libdir)  -L$(shell $(LHAPDF_CONFIG) --libdir) -lLHAPDF
ifeq  ("$(STATIC)","-static") 
## If LHAPDF has been compiled with gfortran and you want to link it statically, you have to include
## libgfortran as well. The same holds for libstdc++. 
## One possible solution is to use fastjet, since $(shell $(FASTJET_CONFIG) --libs --plugins ) -lstdc++
## does perform this inclusion. The path has to be set by the user. 
# LIBGFORTRANPATH= #/usr/lib/gcc/x86_64-redhat-linux/4.1.2
# LIBSTDCPP=/lib64
LIBSLHAPDF+=-L$(LIBGFORTRANPATH) -lgfortranbegin -lgfortran -L$(LIBSTDCPP) -lstdc++
endif
LIBS+=$(LIBSLHAPDF)
else
PDFPACK=mlmpdfif.o hvqpdfpho.o
endif

ifeq ("$(ANALYSIS)","default")
RIVETLIBS=$(shell rivet-config --libs --plugins ) -lHepMCfio -lstdc++
RIVETCXXFLAGS=$(shell rivet-config --cppflags) -std=gnu++11
PWHGANAL=multi_plot.o pwhg_analysis-rivet.o pythiaToHepMCdummy.o powheginput_cpp.o
PWHGANALPY=multi_plot.o pwhg_analysis-rivet.o 
else
RIVETLIBS=
RIVETCXXFLAGS=
PWHGANAL=pwhg_analysis-dummy.o pwhg_bookhist-new.o finalize.o
PWHGANALPY=${PWHGANAL}
endif

ifeq ("$(WITHZLIB)","yes")
LIBS+=-lz
endif

ifeq ("$(WITHZLIB)","dummy")
LIBZDUMMY=zlibdummy.o
endif

%.o: %.f $(INCLUDE) | $(OBJDIR)
	$(FF) -c -o $(OBJ)/$@ $<

%.o: %.F $(INCLUDE) | $(OBJDIR)
	$(FF) -c -o $(OBJ)/$@ $<

%.o: %.f90 $(INCLUDE) | $(OBJDIR)
	$(FF) -c -o $(OBJ)/$@ $<

%.o: %.c | $(OBJDIR)
	$(CC) $(DEBUG) -c -o $(OBJ)/$@ $^ 

%.o: %.cc | $(OBJDIR)
	$(CXX) $(DEBUG) -c -o $(OBJ)/$@ $^ $(FJCXXFLAGS) $(RIVETCXXFLAGS)

%FPIC.o: %.f $(INCLUDE) | $(OBJDIR)
	$(FF) -fPIC -c -o $(OBJ)/$@ $<

USER=

ifdef OLPATH
FF+= -I$(OLPATH)/lib_src/openloops/mod
LIBSOPENLOOPS= -Wl,-rpath=$(PWD)/$(OBJDIR) -L$(PWD)/$(OBJDIR) -lopenloops
USER+=openloops.o
endif

USER+=init_couplings.o init_processes.o Born_phsp.o Born.o virtual.o \
     real.o $(PWHGANAL)

USER+=ttbb_decay.o

# PYTHIA 8
FJCXXFLAGS+=$(shell  pythia8-config --cxxflags)
LIBPYTHIA8= -L$(shell pythia8-config --libdir)  -L$(shell pythia8-config --libdir)/archive -lpythia8 -ldl -lstdc++ -Wl,-rpath,$(shell pythia8-config --libdir) #-llhapdfdummy

PWHG=pwhg_main.o pwhg_init.o bbinit.o btilde.o lhefwrite.o		\
	LesHouches.o LesHouchesreg.o gen_Born_phsp.o find_regions.o	\
	fill_res_histories.o sigequiv_hook.o                         \
	test_Sudakov.o pt2maxreg.o sigborn.o gen_real_phsp.o maxrat.o	\
	gen_index.o gen_radiation.o Bornzerodamp.o sigremnants.o	\
	sigregular.o build_resonance_hists.o resize_arrays.o		\
	random.o boostrot.o bra_ket_subroutines.o cernroutines.o	\
	init_phys.o powheginput.o pdfcalls.o sigreal.o sigcollremn.o	\
	pwhg_analysis_driver.o checkmomzero.o		                \
	setstrongcoupl.o integrator.o mintwrapper.o newunit.o mwarn.o  	\
	sigsoftvirt.o reshufflemoms.o                                 	\
	sigcollsoft.o sigvirtual.o  ubprojections-new.o			\
	resweights.o locks.o genericphsp.o PhaseSpaceUtils.o boostrot4.o\
	setlocalscales.o mint_upb.o opencount.o fullrwgt.o          \
  pwhg_io_interface.o rwl_weightlists.o  \
  rwl_setup_param_weights.o rwl_setup_param_weights_user.o lhefread.o \
	validflav.o cache_similar.o utils.o $(PDFPACK) $(USER) $(FPEOBJ) \
	$(LIBZDUMMY)
# Get SVN info for SVN version stamping code
$(shell ../svnversion/svnversion.sh>/dev/null)

# target to generate LHEF output
pwhg_main:compile_openloops $(PWHG) $(USERLIBS)
	$(FF) $(patsubst %,$(OBJ)/%,$(PWHG)) $(LINKUSERLIBS) $(LIBSOPENLOOPS) $(LIBS) $(LIBSFASTJET) $(RIVETLIBS) $(STATIC) -o $@

LHEF=lhef_analysis.o boostrot4.o utils.o random.o cernroutines.o\
     opencount.o powheginput.o $(PWHGANAL) \
     pwhg_io_interface.o lhefread.o rwl_weightlists.o newunit.o pwhg_analysis_driver.o \
     $(LIBZDUMMY) $(FPEOBJ)

# target to analyze LHEF output
lhef_analysis:$(LHEF)
	$(FF) $(patsubst %,$(OBJ)/%,$(LHEF)) $(LIBS) $(LIBSFASTJET) $(RIVETLIBS) $(STATIC)  -o $@ 


LHEFD=lhef_decay.o pwhg_init.o bbinit.o btilde.o lhefwrite.o		\
	LesHouches.o LesHouchesreg.o gen_Born_phsp.o find_regions.o	\
	fill_res_histories.o sigequiv_hook.o                        \
	test_Sudakov.o pt2maxreg.o sigborn.o gen_real_phsp.o maxrat.o	\
	gen_index.o gen_radiation.o Bornzerodamp.o sigremnants.o	\
	sigregular.o build_resonance_hists.o resize_arrays.o		\
	random.o boostrot.o bra_ket_subroutines.o cernroutines.o	\
	init_phys.o powheginput.o pdfcalls.o sigreal.o sigcollremn.o	\
	pwhg_analysis_driver.o checkmomzero.o		                \
	setstrongcoupl.o integrator.o mintwrapper.o newunit.o mwarn.o  	\
	sigsoftvirt.o reshufflemoms.o                                 	\
	sigcollsoft.o sigvirtual.o  ubprojections-new.o			\
	resweights.o locks.o genericphsp.o PhaseSpaceUtils.o boostrot4.o\
	setlocalscales.o mint_upb.o opencount.o fullrwgt.o          \
  pwhg_io_interface.o rwl_weightlists.o  \
  rwl_setup_param_weights.o rwl_setup_param_weights_user.o lhefread.o \
	validflav.o cache_similar.o utils.o $(PDFPACK) $(USER) $(FPEOBJ) \
	$(LIBZDUMMY)

lhef_decay: compile_openloops $(LHEFD) lhef_decay.o
	$(FF) $(patsubst %,$(OBJ)/%,$(LHEFD)) $(LINKUSERLIBS) $(LIBSOPENLOOPS) $(LIBS) $(LIBSFASTJET) $(RIVETLIBS) $(STATIC) -o $@

# target to read event file, shower events with PYTHIA8.1 + analysis
PYTHIA8=main-PYTHIA8.o boostrot.o powheginput.o \
	$(PWHGANALPY) opencount.o pwhg_io_interface.o lhefread.o rwl_weightlists.o newunit.o pdfdummies.o \
	random.o cernroutines.o bra_ket_subroutines.o boostrot4.o utils.o powheginput_cpp.o locks.o \
	$(FPEOBJ) $(LIBZDUMMY)

# target to read event file, shower events with PYTHIA8.2 + analysis
main-PYTHIA82-lhef: $(PYTHIA8) pythia82F77.o
	$(FF) $(patsubst %,$(OBJ)/%,$(PYTHIA8) pythia82F77.o ) $(LIBSFASTJET) $(RIVETLIBS) $(LIBPYTHIA8) $(STATIC) $(LIBS) -o $@

# build OpenLoops: calls scons
compile_openloops:
ifdef OLPATH
ifdef OLLIBS
	@if [ -d "$(OLPATH)" ]; then \
	echo "Compiling OpenLoops..." ; cd $(OLPATH) ; \
  ./scons generic_lib_dir=$(PWD)/$(OBJ)/ compile_libraries=cuttools,collier link_libraries=cuttools,collier \
  process_src_dir=$(PWD)/OL_process_src \
  process_obj_dir=$(PWD)/$(OBJ)/OL_process_obj \
  process_lib_dir=$(PWD)/$(OBJ)/proclib ; \
	./openloops libinstall $(OLLIBS) generator=0 compile_extra=1 fortran_compiler=$(COMPILER) \
	generic_lib_dir=$(PWD)/$(OBJ)/ compile_libraries=cuttools,collier link_libraries=cuttools,collier \
  process_src_dir=$(PWD)/OL_process_src \
  process_obj_dir=$(PWD)/$(OBJ)/OL_process_obj \
  process_lib_dir=$(PWD)/$(OBJ)/proclib ; \
  echo "...done."; \
	else \
	echo "ERROR: OpenLoops not available for POWHEG. Please download!" ;\
    exit 1; \
	fi
else
	@echo "ERROR compiling OpenLoops: OLLIBS not specified!" ;
endif
else
	@echo "ERROR compiling OpenLoops: OLPATH not specified!" ;
endif

clean:
	rm -f $(OBJ)/*.o $(OBJ)/*.mod $(OBJ)/*.a pwhg_main lhef_analysis lhef_decay 	\
	main-PYTHIA82-lhef *.a

veryclean:
	rm -f $(OBJ)/*.o $(OBJ)/*.mod $(OBJ)/*.a $(OBJ)/*.so pwhg_main lhef_analysis lhef_decay *.a; \
  rm -rf OL_process_obj $(OBJ)/OL_process_obj $(OBJ)/proclib; \
  rm ../OpenLoopsStuff/OpenLoops/.sconsign.dblite; \
  cd $(OLPATH); ./scons -c clean=src

# target to generate object directory if it does not exist
$(OBJDIR):
	mkdir -p $(OBJDIR)

##########################################################################

# Dependencies of SVN version stamp code
pwhg_main.o: svn.version ProcessCitation.f
lhefwrite.o: svn.version

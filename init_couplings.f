      subroutine init_couplings
      implicit none
      include 'PhysPars.h'
      include 'pwhg_st.h'
      include "pwhg_physpar.h"

      complex * 8 cwmass2, czmass2
      integer j, ewscheme

      double precision  Two, Four, Rt2, Pi
      parameter( Two = 2.0d0, Four = 4.0d0 )
      parameter( Rt2   = 1.414213562d0 )
      parameter( Pi = 3.14159265358979323846d0 )

      real * 8 powheginput
      external powheginput
c Avoid multiple calls to this subroutine. The parameter file is opened
c but never closed ...
      logical called
      data called/.false./
      save called
      real * 8 pwhg_alphas, alphasPDF
      external pwhg_alphas, alphasPDF

      real * 8 sampling(50)
      data sampling/1.25892541,1.44272539,1.6533597,1.89474609,
     1              2.1713743,2.48838954,2.85168821,3.26802759,
     1              3.74515148,4.29193426,4.91854597,5.63664143,
     1              6.45957703,7.40265918,8.48342898,9.72198848,
     1              11.14137457,12.7679875,14.63208187,16.76832937,
     1              19.21646368,22.0220195,25.23717948,28.92174481,
     1              33.14424749,37.98322505,43.52868128,49.8837603,
     1              57.16666502,65.51285569,75.0775694,86.03870748,
     1              98.60014441,112.99551984,129.49258422,148.3981789,
     1              170.06394329,194.89285531,223.34672663,255.95479227,
     1              293.32355425,336.14806238,385.224842,441.46670918,
     1              505.91974884,579.78277172,664.42961191,761.43467988,
     1              872.60224609,1000.0/
      integer is

      if(called) then
         return
      else
         called=.true.
      endif


      ph_tmass=powheginput('#tmass')
      if(ph_tmass<0d0) ph_tmass=172.5d0

c default: set top width to zero
c (this value is used in matrix elements)
      ph_twidth = 0d0

      decay_twidth=powheginput('#twidth') ! this value is used for decays
      if(decay_twidth<0d0) decay_twidth = 1.329d0

      ph_bmass=powheginput('#bmass')
      if(ph_bmass<0d0) ph_bmass = 4.75d0



! EW parameters only enter top decays. They do not enter for ttbb @ NLO QCD.
      ph_Zmass=powheginput('#zmass')
      if(ph_Zmass<0d0) ph_Zmass = 91.188d0
      ph_Zwidth=powheginput('#zwidth')
      if(ph_Zwidth<0d0) ph_Zwidth=2.441d0

      ph_Wmass=powheginput('#wmass')
      if(ph_Wmass<0d0) ph_Wmass = 80.385d0
      ph_Wwidth=powheginput('#wwidth')
      if(ph_Wwidth<0d0) ph_Wwidth=2.089d0

      ph_hmass = 125
      ph_hwidth = 0

      ewscheme=powheginput('#ewscheme')
      if(ewscheme<1.or.ewscheme>2) ewscheme = 2

      if (ewscheme.eq.1) then  ! MW, MZ, Gmu scheme
        ph_gfermi=powheginput('#gfermi')
        if(ph_gfermi<0d0) ph_gfermi = 1.1658029175194381d-5
        if (powheginput("#complexGFermi").eq.0) then
          ph_alphaem=sqrt(2d0)/pi*ph_gfermi*abs(ph_Wmass**2*(1d0-(ph_Wmass/ph_Zmass)**2))
          ph_sthw2 = 1. - (ph_Wmass**2/ph_Zmass**2)
        else
          cwmass2=CMPLX(ph_Wmass**2,-ph_Wwidth*ph_Wmass)
          czmass2=CMPLX(ph_Zmass**2,-ph_Zwidth*ph_Zmass)
          ph_alphaem=sqrt(2d0)/pi*ph_gfermi*abs(cwmass2*(1d0-cwmass2/czmass2))
          ph_sthw2 = 1. - abs(cwmass2/czmass2)
        endif
      else  ! MW, MZ, alpha scheme
        ph_alphaem=powheginput('#alpha')
        if(ph_alphaem<0d0) ph_alphaem = 1/132.50698d0
        if (powheginput("#complexGFermi").eq.0) then
          ph_gfermi=ph_alphaem/sqrt(2d0)*pi/(1d0-(ph_Wmass/ph_Zmass)**2)/ph_Wmass**2
          ph_sthw2 = 1. - (ph_Wmass**2/ph_Zmass**2)
        else
          cwmass2=CMPLX(ph_Wmass**2,-ph_Wwidth*ph_Wmass)
          czmass2=CMPLX(ph_Zmass**2,-ph_Zwidth*ph_Zmass)
          ph_gfermi=ph_alphaem/sqrt(2d0)*pi/abs((1d0-cwmass2/czmass2)*cwmass2)
          ph_sthw2 = 1. - abs(cwmass2/czmass2)
        endif
      end if

c CKM elements only enter top decays via 2 generation cabibbo mixing
c

      ph_CKM(1,1)=0.97428d0
      ph_CKM(1,2)=0.2253d0
      ph_CKM(1,3)=0.00347d0
      ph_CKM(2,1)=0.2252d0
      ph_CKM(2,2)=0.97345d0
      ph_CKM(2,3)=0.0410d0
      ph_CKM(3,1)=0.00862d0
      ph_CKM(3,2)=0.0403d0
      ph_CKM(3,3)=0.999152d0


c     Set here lepton and quark masses for momentum reshuffle in the LHE event file
      physpar_ml(1) = 0.51099891d-3
      physpar_ml(2) = 0.1056583668d0
      physpar_ml(3) = 1.77684d0
      physpar_mq(1) = 0.33d0     ! down
      physpar_mq(2) = 0.33d0     ! up
      physpar_mq(3) = 0.50d0     ! strange
      physpar_mq(4) = 1.50d0     ! charm
      physpar_mq(5) = ph_bmass      ! bottom


c set up masses and widths for resonance damping factors & gernic phase space
      physpar_pdgmasses(24) = ph_Wmass
      physpar_pdgmasses(23) = ph_Zmass
      physpar_pdgmasses(5) = ph_bmass
      physpar_pdgmasses(6) = ph_tmass
      physpar_pdgmasses(22) = 0
      physpar_pdgwidths(24) = ph_Wwidth
      physpar_pdgwidths(23) = ph_Zwidth
      physpar_pdgwidths(6) = ph_twidth
      physpar_pdgwidths(22) = 0

      do j=1,physpar_npdg
         physpar_pdgmasses(-j) = physpar_pdgmasses(j)
         physpar_pdgwidths(-j) = physpar_pdgwidths(j)
      enddo

      physpar_phspmasses = physpar_pdgmasses
      physpar_phspwidths = physpar_pdgwidths


      print*,'ph_Zmass = ',ph_Zmass
      print*,'alphasPDF(ph_Zmass) = ',alphasPDF(ph_Zmass)
      print*,'pwhg_alphas(ph_Zmass**2, st_lambda5MSB, -1) = ',pwhg_alphas(ph_Zmass**2, st_lambda5MSB, -1) 
      print*,'pwhg_alphas(ph_Zmass**2, st_lambda5MSB, 4) = ',pwhg_alphas(ph_Zmass**2, st_lambda5MSB, 4) 
      do is=1,50
         print*,sampling(is),alphasPDF(sampling(is)),pwhg_alphas(sampling(is)**2, st_lambda5MSB, 4),pwhg_alphas(sampling(is)**2, st_lambda5MSB, -1)
      enddo

      end

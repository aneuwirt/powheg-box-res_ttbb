
      integer nlegborn,nlegreal
      parameter (nlegborn=         6 )
      parameter (nlegreal=nlegborn+1)

      integer nlegbornexternal
      parameter (nlegbornexternal=nlegborn)
      integer nlegrealexternal
      parameter (nlegrealexternal=nlegreal)


      integer ndiminteg
!     parameter (ndiminteg=(nlegreal-2)*3-4+2-1)
      parameter (ndiminteg=(nlegreal-2)*3-4+2)

      integer maxprocborn,maxprocreal
      parameter (maxprocborn=999,maxprocreal=999)

      integer maxreshists
      parameter (maxreshists=500)

      integer maxalr
      parameter (maxalr=maxprocreal*nlegreal*(nlegreal-1)/2)
